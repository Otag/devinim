# Kamu Gücü Devinimi

![https://gitlab.com/Otag/Dene/badges/master/build.svg?job=test]()

## Tasarı Adı Hakkında
Yunanca kökenli demokrasi sözcüğü gerçekte ön Türkçe kökenlidir ve `Dumu Gir Atuku` yani `Kamu Gücü Devinimi` anlamına gelmektedir.

## Tasarı Açıklaması

Bu tasarı taşınabilir aygıtların tarayıcılarında yerleşik olarak sağlanan HTML5 duyarga(sensör) arabirimlerini kullanarak kullanıcı aygıtlarından gelen yön bilgilerini anlık olarak işlemeyi ve görselleştirmeyi amaçlamaktadır.

Bu verilerin sıklığı ve anlık olarak erişim istemi, gerçek zamanlı iletişim yöntemlerini kullanmayı gerektirmektedir.
